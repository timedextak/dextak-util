<?php

namespace Dextak\Util;

class Video
{

    public function obterCodigoVideoYoutube($codyouvid)
    {

        $parte1 = explode("v=", $codyouvid);

        if ($parte1 != false) {
            $parte2 = explode("&", $parte1[1]);
            return $parte2 [0];
        } else {
            return null;
        }
    }

}