<?php


namespace Dextak\Util;

class UploadGeral
{

    //Função de validação de arquivo
    public function upload($arq, $type_valido, $uploaddir)
    { //$type_valido e um array com as extensoes validas previamente determinadas
        $type_arq = explode(".", $arq['name']);
        if (in_array(end($type_arq), $type_valido)) {
            //faz o upload dos arquivos caso seja validado

            $nome = $uploaddir . $arq['name'] . date('Y-m-d H:i:s');
            $nome = md5($nome);
            $nome = $nome . '.' . end($type_arq);

            $uploadfile = $uploaddir . basename($nome);
            move_uploaded_file($arq['tmp_name'], $uploadfile);
            return $uploadfile;
        } else {
            return (FALSE);
        }
    }

    //Lista todos os arquivos upados no diretório
    public function listar($diretorio)
    {
        $ponteiro = opendir($diretorio);

        while ($nome_itens = readdir($ponteiro)) {

            $file = $diretorio . "/" . $nome_itens;

            if (file_exists($diretorio . "/" . $nome_itens)) {
                if ($file != $diretorio . "/." & $file != $diretorio . "/..") {
                    $material[] = array("diretorio" => $diretorio, "file" => $file, "nome" => $nome_itens);
                }
            }
        }

        if (isset($material)) {
            return $material;
        }
    }

    //deleta o arquivo upado passado no parametro
    public function del($arq)
    {
        if (file_exists($arq)) {
            unlink($arq);
        }
    }

}