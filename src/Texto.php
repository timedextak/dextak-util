<?php

namespace Dextak\Util;

class Texto
{

    public function slug($texto)
    {

        $texto = strtolower($texto);
        $texto = $this->removerAcentos($texto);
        $texto = preg_replace("/[^a-zA-Z0-9\s]/", "", $texto);
        $texto = str_replace(' ', '-', $texto);

        return $texto;
    }

    /** Pede como argumento uma string com acentos e caracteres especiais,
     * e depois remove estes acentos, renomeamento de arquivos. */
    public function removerAcentos($texto)
    {

        $texto = strtolower($texto);
        $texto = str_replace("á", "a", $texto);
        $texto = str_replace("à", "a", $texto);
        $texto = str_replace("â", "a", $texto);
        $texto = str_replace("ã", "a", $texto);
        $texto = str_replace("ê", "e", $texto);
        $texto = str_replace("è", "e", $texto);
        $texto = str_replace("é", "e", $texto);
        $texto = str_replace("í", "i", $texto);
        $texto = str_replace("ì", "i", $texto);
        $texto = str_replace("î", "i", $texto);
        $texto = str_replace("ò", "o", $texto);
        $texto = str_replace("ó", "o", $texto);
        $texto = str_replace("ô", "o", $texto);
        $texto = str_replace("õ", "o", $texto);
        $texto = str_replace("ú", "u", $texto);
        $texto = str_replace("ñ", "n", $texto);
        $texto = str_replace("~", "", $texto);
        $texto = str_replace("^", "", $texto);
        $texto = str_replace("´", "", $texto);
        $texto = str_replace("`", "", $texto);
        $texto = str_replace("ç", "c", $texto);


        return $texto;
    }

    /**
     * Remove os caracteres especiais de uma string
     *
     * @param string $texto string a ser tratada
     * @return sting string tratada
     */
    public function remCaracteresEspeciais($texto)
    {

        $texto = strtolower($texto);
        $texto = str_replace("!", "", $texto);
        $texto = str_replace("@", "", $texto);
        $texto = str_replace("#", "", $texto);
        $texto = str_replace("$", "", $texto);
        $texto = str_replace("%", "", $texto);
        $texto = str_replace("¨", "", $texto);
        $texto = str_replace("&", "", $texto);
        $texto = str_replace("*", "", $texto);
        $texto = str_replace("(", "", $texto);
        $texto = str_replace(")", "", $texto);
        $texto = str_replace("-", "", $texto);
        $texto = str_replace("_", "", $texto);
        $texto = str_replace("+", "", $texto);
        $texto = str_replace("=", "", $texto);
        $texto = str_replace("§", "", $texto);
        $texto = str_replace("[", "", $texto);
        $texto = str_replace("]", "", $texto);
        $texto = str_replace("{", "", $texto);
        $texto = str_replace("}", "", $texto);
        $texto = str_replace("ª", "", $texto);
        $texto = str_replace("º", "", $texto);
        $texto = str_replace("?", "", $texto);
        $texto = str_replace("/", "", $texto);
        $texto = str_replace("°", "", $texto);
        $texto = str_replace("\\", "", $texto);
        $texto = str_replace("|", "", $texto);
        $texto = str_replace(",", "", $texto);
        $texto = str_replace(".", "", $texto);
        $texto = str_replace(";", "", $texto);
        $texto = str_replace(":", "", $texto);
        $texto = str_replace(">", "", $texto);
        $texto = str_replace("<", "", $texto);
        $texto = str_replace("/", "", $texto);
        $texto = str_replace("*", "", $texto);
        $texto = str_replace("-", "", $texto);
        $texto = str_replace("+", "", $texto);

        return $texto;
    }

    /** Limita o número de caracteres de uma string sem deixar
     * palavras cortadas */
    public function limitarCaracteres($string, $length = 150)
    {
        if (strlen($string) <= $length)
            return $string;
        $corta = substr($string, 0, $length);
        $espaco = strrpos($corta, ' ');
        return substr($string, 0, $espaco);
    }

    /**
     * Extrai somente o texto de formatações com separações tipo '#@#'
     */
    public function extrairTexto($texto, $ini = '<p>', $fim = '</p>')
    {

        $conpos = explode('#@#', $texto);

        $con = '';

        foreach ($conpos as $valor) {

            if ($valor != '') {

                $c = explode('=', $valor);

                if ($c [0] == 'texto') {

                    $con .= $ini . $c [1] . $fim;
                }
            }
        }

        return $con;
    }

    public function mask($val, $mask)
    {
        $maskared = '';
        $k = 0;
        for ($i = 0; $i <= strlen($mask) - 1; $i++) {
            if ($mask[$i] == '#') {
                if (isset($val[$k]))
                    $maskared .= $val[$k++];
            } else {
                if (isset($mask[$i]))
                    $maskared .= $mask[$i];
            }
        }
        return $maskared;
    }

    public function extractCommonWords($string)
    {
        $stopWords = array('i', 'a', 'about', 'an', 'and', 'are', 'as', 'at', 'be', 'by', 'com', 'de', 'en', 'for', 'from', 'how', 'in', 'is', 'it', 'la', 'of', 'on', 'or', 'that', 'the', 'this', 'to', 'was', 'what', 'when', 'where', 'who', 'will', 'with', 'und', 'the', 'www');

        $string = preg_replace('/\s\s+/i', '', $string); // replace whitespace
        $string = trim($string); // trim the string
        $string = preg_replace('/[^a-zA-Z0-9 -]/', '', $string); // only take alphanumerical characters, but keep the spaces and dashes too…
        $string = strtolower($string); // make it lowercase

        preg_match_all('/\b.*?\b/i', $string, $matchWords);
        $matchWords = $matchWords[0];

        foreach ($matchWords as $key => $item) {
            if ($item == '' || in_array(strtolower($item), $stopWords) || strlen($item) <= 3) {
                unset($matchWords[$key]);
            }
        }
        $wordCountArr = array();
        if (is_array($matchWords)) {
            foreach ($matchWords as $key => $val) {
                $val = strtolower($val);
                if (isset($wordCountArr[$val])) {
                    $wordCountArr[$val]++;
                } else {
                    $wordCountArr[$val] = 1;
                }
            }
        }
        arsort($wordCountArr);
        $wordCountArr = array_slice($wordCountArr, 0, 10);
        return $wordCountArr;
    }

}